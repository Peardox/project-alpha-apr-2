from django.views.generic.list import ListView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView
from projects.models import Project
from tasks.models import Task

from django.urls import reverse_lazy, reverse


class ProjectListView(LoginRequiredMixin, ListView):
    model = Project
    template_name = "projects/list.html"
    context_object_name = "Projects"

    def get_queryset(self):
        return Project.objects.filter(members=self.request.user)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        print(context)
        return context


class ProjectDetailView(LoginRequiredMixin, DetailView):
    model = Project
    template_name = "projects/detail.html"
    context_object_name = "Project"

    def get_queryset(self):
        return Project.objects.filter(members=self.request.user)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        print(context)
        return context


class ProjectCreateView(LoginRequiredMixin, CreateView):
    model = Project
    template_name = "projects/create.html"
    fields = ["name", "description", "members"]
    success_url: reverse_lazy("show_project")

    def form_valid(self, form):
        item = form.save()
        self.pk = item.pk
        return super(ProjectCreateView, self).form_valid(form)

    def get_success_url(self) -> str:
        return reverse("show_project", args=[self.pk])


class TaskCreateView(LoginRequiredMixin, CreateView):
    model = Task
    template_name = "projects/create.html"
    fields = ["name", "start_date", "due_date", "project", "assignee"]
    success_url: reverse_lazy()

    def form_valid(self, form):
        object = form.save()
        self.pk = object.pk
        return super(TaskCreateView, self).form_valid(form)

    def get_success_url(self) -> str:
        return reverse("show_project", args=[self.pk])


class TaskListView(LoginRequiredMixin, ListView):
    model = Task
    template_name = "tasks/list.html"
    context_object_name = "Tasks"

    def get_queryset(self):
        return Task.objects.filter(assignee=self.request.user)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        print(context)
        return context


class TaskUpdateView(UpdateView):
    model = Task
    template_name = "tasks/list.html"
    fields = ["is_completed"]
    success_url: reverse_lazy()

    def get_queryset(self):
        return Task.objects.filter(assignee=self.request.user)

    def get_success_url(self):
        return reverse_lazy("show_my_tasks", args=["tasks/list.html"])
